productData = [
  {
    "productId": 1000,
    "productName": 'Product 1000'
  },
  {
    "productId": 1001,
    "productName": 'Product 1001'
  }
]

stockData = [
  {
    "productId": 1000,
    "locationId": 1,
    "stock": 21
  },
  {
    "productId": 1000,
    "locationId": 2,
    "stock": 8
  },
  {
    "productId": 1001,
    "locationId": 1,
    "stock": 4
  },
  {
    "productId": 1001,
    "locationId": 2,
    "stock": 10
  }
]

locationData = [
  {
    "locationId": 1,
    "locationName": 'Location 1'
  },
  {
    "locationId": 2,
    "locationName": 'Location 2'
  }
]


def transform_product_data(product_data, loc_data, stock_data):

    # transform list of object into key-value pairs
    _loc_data: dict = { i.get('locationId'): i.get('locationName') for i in loc_data}

    def group_detail_based_on_product():
        return [
            {
                **pd, 
                "details": [i for i in stock_data if i.get("productId") == pd.get("productId")]
            } 
            for pd in product_data
        ]

    def calculate_total_stock(detail: list) -> int:
        total = 0
        for x in detail:
            total = total + x.get('stock', 0)
        
        return total

    def build_detail_data(detail: list) -> list:
        # detail data only consist of stock and locationId
        key_need_to_be_removed: list = ['locationId', 'productId']
        for i in detail:
            i['locationName'] = _loc_data.get(i.get('locationId'))
            for j in key_need_to_be_removed:
                if j in i:
                    i.pop(j, None)

    def build_stock_ctx(detail: list) -> dict:
        """
        stock context consist of
        1) detail: detail data has been cleaned
        2) total: sum of stock per product
        """
        build_detail_data(detail)
        total = calculate_total_stock(detail)
        return  \
            {
                "total": total,
                "detail": detail
            }

    # group the detail data based on product
    products = group_detail_based_on_product()
    # after data was grouped, we need to remove the unncessary attribute
    # and calculate total as derivative attribute of sum of stock per product
    # the benefit of grouping the context product at first is we can do palallel processing then /
    # in the future build_detail_data, in this context calculatting sum of stock and compose detail data
    return \
        [
            { 
                "productName": i.get('productName'), 
                "stock": build_stock_ctx(i.get('details')) 
            } 
            for i in products
        ]


assert \
[
  {
    'productName': 'Product 1000',
    'stock': {
      'total': 29,
      'detail': [
        {
          'locationName': 'Location 1',
          'stock': 21
        },
        {
          'locationName': 'Location 2',
          'stock': 8
        }
      ]
    }
  },
  {
    'productName': 'Product 1001',
    'stock': {
      'total': 14,
      'detail': [
        {
          'locationName': 'Location 1',
          'stock': 4
        },
        {
          'locationName': 'Location 2',
          'stock': 10
        }
      ]
    }
  }
] == transform_product_data(productData, locationData, stockData)

print('Success')
 